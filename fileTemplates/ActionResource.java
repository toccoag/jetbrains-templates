#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end

import ch.tocco.nice2.rest.action.spi.AbstractActionResource;
import ch.tocco.nice2.rest.action.spi.TaskContext;
import ch.tocco.nice2.rest.action.spi.beans.ActionDataBean;
import ch.tocco.nice2.rest.action.spi.beans.ActionResourceBean;
import ch.tocco.nice2.rest.core.api.RestResource;
import ch.tocco.nice2.security.api.Secured;
import ch.tocco.nice2.tasks.api.quartz.AbstractJob;
import ch.tocco.nice2.textresources.api.TextMessage;

import jakarta.ws.rs.Path;

@Path("/actions/${PATH}")
@Secured(roles = "${ROLES}")
@RestResource
public class ${NAME}ActionResource extends AbstractActionResource {
    protected ${NAME}ActionResource() {
        super(SelectionType.MULTIPLE);
    }

    @Override
    protected ActionResultBean doPerformAction(ActionDataBean actionDataBean, ActionResourceBean actionResourceBean, TaskContext taskContext) {
        return new ActionResultBeanBuilder(true).build();
    }

    @Override
    protected TextMessage actionName() {
        return new TextMessage("actions.${NAME}");
    }

    @Override
    protected Class<? extends AbstractJob> getJobClass() {
        return ${NAME}Job.class;
    }

    private class ${NAME}Job extends AbstractActionJob {
    }
}
