#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end

import ch.tocco.nice2.tasks.api.quartz.AbstractJob;
import ch.tocco.nice2.tasks.api.quartz.BatchJob;

import ch.tocco.nice2.tasks.api.quartz.JobDataMapReader;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@BatchJob(
    id = "${NAME}Batchjob",
    description = "description",
    schedule = "schedule"
)
@DisallowConcurrentExecution
public class ${NAME}Batchjob extends AbstractJob {
    @Override
    protected void doExecute(JobExecutionContext context, JobDataMapReader jobDataMapReader) throws JobExecutionException {
    }
}
