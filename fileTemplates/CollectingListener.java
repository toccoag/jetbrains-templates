#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end

import ch.tocco.nice2.boot.api.ThreadScope;
import ch.tocco.nice2.persist.core.api.entity.events.Listener;
import ch.tocco.nice2.persist.core.api.tx.Transaction;
import ch.tocco.nice2.persist.core.api.util.CollectingEntityListener;

@Listener(filter = "${MODEL}")
@ThreadScope
public class ${NAME}Listener extends CollectingEntityListener {
    @Override
    public void onBeforeCommit(Transaction tx) {
    }
}
