#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end

import javax.annotation.Resource;

import org.testng.annotations.*;

import ch.tocco.nice2.model.entity.api.DataModel;
import ch.tocco.nice2.persist.core.test.inject.model.AbstractDataModel;
import ch.tocco.nice2.persist.core.test.inject.easytest.testcase.EasyValidatorTestCase;
import ch.tocco.nice2.textresources.api.i18n.L10N;
import ch.tocco.nice2.types.api.TypeManager;

import static org.easymock.EasyMock.*;

public class ${NAME}Test extends EasyValidatorTestCase<${NAME}> {

    @Override
    protected Class<? extends DataModel> defineDataModel() {
        return TestDataModel.class;
    }

    @Override
    protected ${NAME} instantiateClassToTest() {
        return new ${NAME}();
    }

    private static class TestDataModel extends AbstractDataModel {
        public TestDataModel(TypeManager typeManager, L10N l10n) {
            super(typeManager, l10n);

            commit();
        }
    }
}

