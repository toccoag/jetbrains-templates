#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end

import java.util.List;

import org.testng.annotations.*;

import org.jetbrains.annotations.Nullable;

import ch.tocco.nice2.model.entity.api.DataModel;
import ch.tocco.nice2.persist.core.test.inject.model.AbstractDataModel;
import ch.tocco.nice2.rest.core.spi.AbstractRestResource;
import ch.tocco.nice2.rest.core.test.EasyJerseyTestCase;
import ch.tocco.nice2.textresources.api.i18n.L10N;
import ch.tocco.nice2.types.api.TypeManager;

import static com.google.common.truth.Truth.*;
import static org.easymock.EasyMock.*;

public class ${NAME}Test extends EasyJerseyTestCase {
    @Override
    protected List<AbstractRestResource> getRestResources() {
        return List.of(new ${NAME}());
    }

    @Override
    protected @Nullable Class<? extends DataModel> defineDataModel() {
        return TestDataModel.class;
    }

    private static class TestDataModel extends AbstractDataModel {
        protected TestDataModel(TypeManager typeManager, L10N l10n) {
            super(typeManager, l10n);

            commit();
        }
    }
}
