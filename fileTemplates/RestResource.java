#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end

import ch.tocco.nice2.rest.core.api.RestResource;
import ch.tocco.nice2.rest.core.spi.AbstractRestResource;
import ch.tocco.nice2.security.api.Secured;

import jakarta.ws.rs.Path;

@Path("/${PATH}")
@Secured(roles = "${ROLES}")
@RestResource
public class ${NAME}Resource extends AbstractRestResource {
}
