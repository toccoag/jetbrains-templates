#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end

import org.springframework.context.annotation.Bean;

import ch.tocco.nice2.appmodule.api.AbstractConfiguration;
import ch.tocco.nice2.boot.api.ToccoSpringBootApplication;
import ch.tocco.nice2.reporting.api.ReportDescription;
import ch.tocco.nice2.reporting.api.description.BusinessUnitCorporateDesignContribution;
import ch.tocco.nice2.reporting.api.description.CorporateDesignContribution;
import ch.tocco.nice2.reporting.api.description.OutputTemplateContributionsBuilder;
import ch.tocco.nice2.reporting.api.description.OutputTemplateFieldContributionsBuilder;
import ch.tocco.nice2.reporting.api.description.ReportContributionsBuilder;
import ch.tocco.nice2.reporting.api.description.TemplateSnippetContributionsBuilder;

@ToccoSpringBootApplication
public class ${NAME} extends AbstractConfiguration {
    @Bean
    public ReportContributionsBuilder ${CUSTOMER}ReportContributions() {
        return ReportContributionsBuilder.create()
            .module(currentModule())
            .id("report_id")
            .label("report.report_id")
            .synchronize(true)
            .outputTemplate("output_template_id")
            .roles("INSERT ROLES")
            .outputLanguagePath("relCorrespondence_language")
            .addPlacementConfig(new ReportDescription.PlacementConfig("INSERT MODEL", "list,detail"))
            .addRecipientConfig(ReportDescription.RecipientConfig.create("."));
    }
    
    @Bean
    public OutputTemplateContributionsBuilder ${CUSTOMER}OutputTemplateContributions() {
        return OutputTemplateContributionsBuilder.create()
            .uniqueId("output_template_id")
            .label("outputtemplate.output_template_id")
            .freemarker(findModelResource("outputtemplate/output_template_id.ftl"))
            .less(findModelResource("outputtemplate/output_template_id.less"))
            .outputTemplateFormat("a4_portrait")
            .outputTemplateLayout("header_footer")
            .outputTemplateUsage("correspondence")
            .fileFormat("wkhtmltopdf.pdf");
    }

    @Bean
    public TemplateSnippetContributionsBuilder ${CUSTOMER}TemplateSnippetsContributions() {
        return TemplateSnippetContributionsBuilder.create()
            .uniqueId("snippet_id")
            .label("templatesnippet.snippet_id")
            .freemarker(findModelResource("templatesnippet/snippet_id.ftl"));
    }

    @Bean
    public OutputTemplateFieldContributionsBuilder ${CUSTOMER}OutputTemplateFieldContributions() {
        return OutputTemplateFieldContributionsBuilder.create()
            .uniqueId("field_id")
            .outputTemplate("output_template_id")
            .title("output_template.field_id")
            .sorting(10)
            .freemarker(findModelResource("outputtemplate/fields/output_template_id.field_id.ftl"));
    }

    @Bean
    public CorporateDesignContribution ${CUSTOMER}CorporateDesign() {
        CorporateDesignContribution bean = new CorporateDesignContribution();
        bean.setUniqueId("${CUSTOMER}");
        bean.setLabelTextResourceKey("corporatedesign.${CUSTOMER}");
        bean.setLess(findModelResource("corporatedesign/${CUSTOMER}.less"));
        return bean;
    }

    @Bean
    public BusinessUnitCorporateDesignContribution ${CUSTOMER}BusinessUnitCorporateDesign() {
        BusinessUnitCorporateDesignContribution bean = new BusinessUnitCorporateDesignContribution();
        bean.setOutputTemplate("letter");
        bean.setCorporateDesign("${CUSTOMER}");
        bean.setBusinessUnits("${CUSTOMER}");
        return bean;
    }
}
