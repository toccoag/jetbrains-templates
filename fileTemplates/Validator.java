#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end

import ch.tocco.nice2.persist.core.api.entity.Entity;
import ch.tocco.nice2.persist.core.api.validation.AbstractEntitiesValidator;
import ch.tocco.nice2.persist.core.api.validation.Validator;
import ch.tocco.nice2.validate.api.result.EntityValidationResult;

import java.util.List;
import java.util.Map;

@Validator(filter = "${MODEL}")
public class ${NAME}Validator extends AbstractEntitiesValidator {
    @Override
    public void validate(List<Entity> entities, Map<Entity, EntityValidationResult> validationResults) {
    }
}
