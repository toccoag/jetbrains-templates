# Tocco Jetbrains Templates

Simply copy the contents of `templates` and `fileTemplates` to their respective counterparts in the config folder of the
tool you want to use them in.

Example target path for IntelliJ Idea 2023.2 on Ubuntu: `/home/luca/.config/JetBrains/IntelliJIdea2023.2`

## Available Live Templates

Many of these live templates depend on some services being available, but can not inject anything themselves. You might
need to add them by hand if you do not have already. Just fix anything that complains.

### General

- addRel
  - resolve and add to relation
- createPk
  - create PrimaryKey from string for some model
- finEnt
  - finalize entity by setting defaults and setting non-nullable to empty 
- setRel
  - resolve and set relation
- hibernateProps
  - create necessary hibernate props (serverName, databaseName, user, password), scoped to *.properties files only.
- newCustomer
  - create a new ansible customer with one installation and sensible defaults. To be used in config.yml of our ansible repo.

### Invoker

- andInv
  - chain another invoker
- inv
  - invoke without return
- invReturn
  - invoke with return
- invReturnWrap
  - wrap selection in invoker with return
- invWrap
  - wrap selection in invoker without return
- priv
  - privileged invoker
- tx
  - transaction invoker
- withBu
  - fixed business unit invoker
- withEntBu
  - entity business unit invoker
- withNullBu
  - null business unit invoker

### Testing

- entModel
  - mock entity model in data model
- lookupCreator
  - create lookup entity task
- stpmck
  - setup a mock for a given class
- testEntity
  - create method for entity creation
- testMethod
  - create test method
- testModel
  - create boilerplate data model
- withField
  - add field mock to entity model

### REST

- endpointGET
  - create GET endpoint
- endpointJSON
  - create POST endpoint with JSON in- and outputs

### Menu

- menu
  - add menu entry
- menuEntity
  - add entity explorer entry

### Contributions

- constrContribution
  - add bean for constriction contribution

## Available File Templates

- ActionResource.java
- ActionResourceTestCase.java
- Batchjob.java
- BatchjobTestCase.java
- CollectingListener.java
- CustomerApplication.java
  - Contains boilerplate for the different report, output template and corporate design contributions 
- CustomerGradle.gradle
  - minimal gradle file for a new customer module
- DetailForm.xml
- EasyTestCase.java
- ListenerTestCase.java
- ListForm.xml
- LookupEntity.xml
- Menu.xml
- Relation.xml
- RestResource.java
- RestResourceTestCase.java
- SearchForm.xml
- Validator.java
- ValidatorTestCase.java